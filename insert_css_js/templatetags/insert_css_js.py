# encoding: utf-8

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from django.conf import settings
from django import template
from django.templatetags.static import static


register = template.Library()


@register.simple_tag
def insert_js(name):
    if settings.DEBUG:
        src = "js/%s.js" % name
    else:
        src = "js/%s.min.js" % name
    return '<script src="{src}" type="text/javascript"></script>'.format(src=static(src))


@register.simple_tag
def insert_css(name):
    if settings.DEBUG:
        src = "css/%s.css" % name
    else:
        src = "css/%s.min.css" % name
    return '<link rel="stylesheet" type="text/css" href="{src}">'.format(src=static(src))
