import os
from setuptools import setup


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

install_requires = [
    'django>=1.7.0',
]

setup(
    name='django_insert_css_js',
    version='0.1',
    packages=['insert_css_js'],
    description='A simple Django app to insert css and js.',
    classifiers=[
        'Framework :: Django',
    ],
)
